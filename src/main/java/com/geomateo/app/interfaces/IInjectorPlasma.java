package com.geomateo.app.interfaces;

public interface IInjectorPlasma {

	Integer calcularCostoAdicional(Integer capacidadAdicionalFlujoPlasma);
	
}
