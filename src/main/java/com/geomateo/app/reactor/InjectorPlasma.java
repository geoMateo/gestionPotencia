package com.geomateo.app.reactor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.geomateo.app.enums.TipoInjector;
import com.geomateo.app.interfaces.IInjectorPlasma;

public class InjectorPlasma extends Injector implements IInjectorPlasma {

	private static final Logger LOGGER = LoggerFactory.getLogger(InjectorPlasma.class);
	
	private TipoInjector tipoInjector;
	
	@Override
	public Integer calcularCostoAdicional(Integer capacidadAdicionalFlujoPlasma) {
		LOGGER.info("Ingresando a calcular costo adicional con capacidadAdicionalFlujoPlasma: {}",
				capacidadAdicionalFlujoPlasma);
		this.capacidadAdicionalFlujoPlasma = capacidadAdicionalFlujoPlasma;
		if (capacidadAdicionalFlujoPlasma > 0) {
			this.tiempoFuncionamiento = CIEN - capacidadAdicionalFlujoPlasma;
			this.consumo = this.consumo + capacidadAdicionalFlujoPlasma;
		}
		return consumo;
	}

	public InjectorPlasma() {
		super();
		tipoInjector = TipoInjector.CODIGO;
	}

	public InjectorPlasma(Integer porcentajeDanado) {
		super(porcentajeDanado);
	}

	public TipoInjector getTipoInjector() {
		return tipoInjector;
	}

	public void setTipoInjector(TipoInjector tipoInjector) {
		this.tipoInjector = tipoInjector;
	}
	
	
}
