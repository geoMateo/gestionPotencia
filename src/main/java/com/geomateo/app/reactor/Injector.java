package com.geomateo.app.reactor;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Injector  {

	private static final Logger LOGGER = LoggerFactory.getLogger(Injector.class);

	public static final Integer CIEN = 100;
	
	/**
	 * La capacidad maxima adicional debe ser hasta 99 mg/g por encima de su
	 * capacidad normal
	 */
	public Integer capacidadAdicionalFlujoPlasma = 0;// mg/s

	/**
	 * Tiempo de funcionamiento el 99999 representa infinito
	 */
	public Integer tiempoFuncionamiento = 99999; // minutos

	/**
	 * Por cada punto de daño, baja un mg/s Si está dañado al 70%, su flujo máximo
	 * indefinido es 30 mg/s. Si está dañado al 32%, el flujo máximo es de 68 mg/s.
	 */
	public Integer porcentajeDanado = 0; // %

	public Integer consumoNormal = CIEN; // mg/s

	public Integer consumo = 0; // mg/s

	public Integer relacionDanoConsumo = 1 / 1;// 1 mg a 1 punto de daño (1% daño)

	public Injector() {
		this.consumo = consumoNormal;
	}

	/**
	 * Calculo para el porcentaje dañado de injector plasma
	 * @param porcentajeDanado
	 */
	public Injector(Integer porcentajeDanado) {
		LOGGER.info("Ingresando a injector plasma con porcentaje dañado: {}", porcentajeDanado);
		this.porcentajeDanado = porcentajeDanado;
		if (porcentajeDanado > 99) {
			this.consumo = 0;
			this.tiempoFuncionamiento = 0;
		} else {
			this.consumo = this.consumoNormal - porcentajeDanado * this.relacionDanoConsumo;
		}
	}

	public void setConsumo(Integer consumo) {
		LOGGER.info("Ingresando a calcular el consumo: {}", consumo);
		this.consumoNormal = this.consumoNormal - consumo;
		if (this.porcentajeDanado > 99) {
			this.consumo = 0;
			this.tiempoFuncionamiento = 0;
		} else {
			this.consumo = this.consumoNormal - this.porcentajeDanado * this.relacionDanoConsumo;
		}
	}

	public Integer getCapacidadAdicionalFlujoPlasma() {
		return capacidadAdicionalFlujoPlasma;
	}

	public Integer getTiempoFuncionamiento() {
		return tiempoFuncionamiento;
	}

	public Integer getPorcentajeDanado() {
		return porcentajeDanado;
	}

	public Integer getConsumoNormal() {
		return consumoNormal;
	}

	public Integer getConsumo() {
		return consumo;
	}

	public String toString() {
		return "Flujo Máximo: " + consumo + " Porcentaje dañado : " + porcentajeDanado + " Tiempo funcionamiento: "
				+ tiempoFuncionamiento + " Inyectando: " + capacidadAdicionalFlujoPlasma;

	}

}
