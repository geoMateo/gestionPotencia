package com.geomateo.app.reactor;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Reactor {

	private static final Logger LOGGER = LoggerFactory.getLogger(Reactor.class);
	private static final String UNABLE_COMPLY= "Unable to comply";
	private static final Integer CIEN = 100;
	private List<InjectorPlasma> injectores;
	private Integer porcentajeVelocidad;
	private Integer totalFlujo = 0;
	private Integer velocidadLuz = 300;

	public Reactor(List<InjectorPlasma> injectores, Integer porcentajeVelocidad) throws Exception {
		this.injectores = injectores;
		this.porcentajeVelocidad = porcentajeVelocidad;
		this.totalFlujo = injectores.stream().mapToInt(Injector::getConsumo).sum();
		Integer flujoNecesario = porcentajeVelocidad * this.velocidadLuz / CIEN;
		if (flujoNecesario > this.totalFlujo) {
			if ((flujoNecesario - this.totalFlujo) % this.injectores.size() == 0) {
				Integer flujoFaltante = (flujoNecesario - this.totalFlujo) / this.injectores.size();
				injectores.stream().forEach(item -> item.calcularCostoAdicional(flujoFaltante));
			} else {
				LOGGER.info(UNABLE_COMPLY);
				throw new Exception(UNABLE_COMPLY);
			}
		}
		if (flujoNecesario < this.totalFlujo) {
			if ((this.totalFlujo - flujoNecesario) % this.injectores.size() == 0) {
				Integer flujoExcede = (this.totalFlujo - flujoNecesario) / this.injectores.size();
				injectores.stream().forEach(item -> item.setConsumo(flujoExcede));
			} else {
				LOGGER.info(UNABLE_COMPLY);
				throw new Exception(UNABLE_COMPLY);
			}
		}
	}

	public List<InjectorPlasma> getInjectores() {
		return injectores;
	}

	public void addInjector(InjectorPlasma injectorPlasma) {
		this.injectores.add(injectorPlasma);
		this.totalFlujo = this.injectores.stream().mapToInt(Injector::getConsumo).sum();
		Integer flujoNecesario = this.porcentajeVelocidad * this.velocidadLuz / CIEN;
		if (flujoNecesario > this.totalFlujo) {
			Integer flujoFaltante = (flujoNecesario - this.totalFlujo) / this.injectores.size();
			this.injectores.stream().forEach(item -> item.calcularCostoAdicional(flujoFaltante));

		}
	}

	public String toString() {
		StringBuilder resultado = new StringBuilder("Reactor: ");
		for (Injector injectorPlasma : this.injectores) {
			resultado.append("\n");
			resultado.append(injectorPlasma.toString());
		}
		return resultado.toString();
	}
}
