package com.geomateo.app.enums;

public enum TipoInjector {

	PLASMA("PLASMA",1),
	CODIGO("PLM",2);
	
	private String tipo;
	private int codigo;
	private TipoInjector(String tipo, int codigo) {
		this.tipo = tipo;
		this.codigo = codigo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
		
}
