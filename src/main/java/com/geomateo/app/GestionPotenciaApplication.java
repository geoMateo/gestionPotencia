package com.geomateo.app;

import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.geomateo.app.reactor.InjectorPlasma;
import com.geomateo.app.reactor.Reactor;

@SpringBootApplication
public class GestionPotenciaApplication implements CommandLineRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(GestionPotenciaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(GestionPotenciaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		LOGGER.info("HOLA ESTA ES UNA MISION SECRETA");

		InjectorPlasma injectorPlasma1 = new InjectorPlasma();

		InjectorPlasma injectorPlasma2 = new InjectorPlasma();

		InjectorPlasma injectorPlasma3 = new InjectorPlasma(30);

		List<InjectorPlasma> injectores = Arrays.asList(injectorPlasma1, injectorPlasma2, injectorPlasma3);

		Reactor reactor = new Reactor(injectores, 140);
		LOGGER.info("\nRESPUESTA: {} ", reactor);
		
	}

}
