package com.geomateo.app.reactor;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class InjectorTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(InjectorTest.class);

	@Test
	void calcularCostoAdicionalTest() {
		InjectorPlasma injectorPlasma = new InjectorPlasma(30);
		Integer costoAdicional = injectorPlasma.calcularCostoAdicional(140);
		LOGGER.info("Costo adicional: {}", costoAdicional);
		assertNotNull(costoAdicional);
	}
	
}
