# Gestón Potencia (CTS)
 El proyecto Gestión Potencia se encarga de gestionar el mejor balance de los injectores de un motor.


## Compilación 🖇️

Se debe compilar el proyecto para que las dependencias especificadas en el pom se descarguen.

1. Clonar el proyecto sri-cts

```
https://gitlab.com/geoMateo/gestionPotencia.git
```
2. Ingresar a la carpeta del proyecto

```
cd gestionPotencia
```
3. Hacer checkout de la rama de master

```
git checkout master
```

4. Compilar

```
mvn clean install
```

## Ejecutar ejercicio ⌨️

Dirigirse a la clase: **GestionPotenciaApplication**

* Crear los Injectores y agregarlos a una lista,
  en el constructor de cada injector se debe anotar el porcentage dañado.

		InjectorPlasma injectorPlasma1 = new InjectorPlasma();

		InjectorPlasma injectorPlasma2 = new InjectorPlasma();

		InjectorPlasma injectorPlasma3 = new InjectorPlasma(30);

		List<InjectorPlasma> injectores = Arrays.asList(injectorPlasma1, injectorPlasma2, injectorPlasma3);

* Crear reactor y agregar lista de injectores y velocidad de la luz,
  En el constructor del Reactor se debe enviar la lista de injectores y la velocidad de luz

		Reactor reactor = new Reactor(injectores, 140);
		LOGGER.info("\nRESPUESTA: {} ", reactor.toString());

* Para ejecutar el proyecto de debe dar click derecho en la clase **GestionPotenciaApplication**
  dar click en opción **Run As** luego a **Java Aplication**
		``` 
		
		Flujo Máximo: 150 Porcentaje dañado : 0 Tiempo funcionamiento: 50 Inyectando: 50
		   		
		Flujo Máximo: 150 Porcentaje dañado : 0 Tiempo funcionamiento: 50 Inyectando: 50
		
		Flujo Máximo: 120 Porcentaje dañado : 30 Tiempo funcionamiento: 50 Inyectando: 50
  		
## Construido con 🛠️

_Las herramientas utilizadas para la construcción del proyecto son_

* [Spring Boot](https://spring.io/projects/spring-boot) - El framework web usado
* [Maven](https://maven.apache.org/) - Manejador de dependencias

## Autores ✒️

_A continuación se listan las personas que colaboraron el la realización del proyecto_

* **José Gómez** - *Trabajo Inicial* - [geoMateo](https://gitlab.com/geoMateo)

 









 

 

 
 
 
 















